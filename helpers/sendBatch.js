const AWS = require("aws-sdk");
const json = require("../myJsonFile");
require("dotenv").config();
const getSSMParameters = require("../config/index");

const _ = require("lodash");

const send = async () => {
  const receivedParams = await getSSMParameters();
  console.log(receivedParams.Parameters);
  AWS.config.update({
    region: receivedParams.Parameters[3].Value,
    accessKeyId: receivedParams.Parameters[0].Value, // should be:  process.env.AWS_ACCESS_ID
    secretAccessKey: receivedParams.Parameters[4].Value,
  });
  // Create SQS service client
  const sqs = new AWS.SQS({ apiVersion: "2012-11-05" });
  // Replace with your account id and the queue name you setup
  const accountId = receivedParams.Parameters[1].Value;
  const queueName = receivedParams.Parameters[2].Value;

  // Setup the sendMessage parameter object
  const sendBatch = async () => {
    //пачками по 10
    const array = _.chunk(json, 10).map((batch) => {
      const params = {
        Entries: batch,
        QueueUrl: `https://sqs.eu-central-1.amazonaws.com/${accountId}/${queueName}`,
      };
      return sqs.sendMessageBatch(params, (err, data) => {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Successfully added message", data.MessageId);
        }
      });
    });
  };
  sendBatch();
};

module.exports = send;
