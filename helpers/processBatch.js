// Load the AWS SDK for Node.js
require("dotenv").config();
const { ConnectContactLens } = require("aws-sdk");
const AWS = require("aws-sdk");
const getSSMParameters = require("../config/index");
const deleteMessage = require("../helpers/deleteMessage");
const sleep = require("../helpers/sleep");

const pool = [];

const process = async (workerI, sqs) => {
  pool[workerI] = false;
  const receivedParams = await getSSMParameters();
  AWS.config.update({
    region: receivedParams.Parameters[3].Value,
    accessKeyId: receivedParams.Parameters[0].Value, // should be:  process.env.AWS_ACCESS_ID
    secretAccessKey: receivedParams.Parameters[4].Value,
  });
  // Create SQS service object

  // Replace with your accountid and the queue name you setup
  const accountId = receivedParams.Parameters[1].Value;
  const queueName = receivedParams.Parameters[2].Value;
  const queueUrl = `https://sqs.eu-central-1.amazonaws.com/${accountId}/${queueName}`;
  // Setup the receiveMessage parameters

  const params = {
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 10,
    VisibilityTimeout: 20,
    WaitTimeSeconds: 20,
  };

  const processBatch = () => {
    sqs.receiveMessage(params, async (err, data) => {
      if (err) {
        console.log(err, err.stack);
      } else {
        if (!data.Messages) console.log("No messages to process");
        else {
          //const orderData = JSON.parse(data.Messages[0].Body);
          const orderData = data.Messages.map((message) => {
            return {
              Id: message.MessageId,
              ReceiptHandle: message.ReceiptHandle,
            };
          });
          //   console.log("Order received", orderData);
          const deletedData = orderData.map(async (message) => {
            await sleep(2000);
            deleteMessage(queueUrl, message.ReceiptHandle, message.Id);
          });
          Promise.all(deletedData).then(console.log("done"));
          pool[workerI] = true;
        }
      }
    });
  };
  processBatch();
};

const mainFunction = () => {
  for (let i = 0; i < 10; i++) pool[i] = true;

  function worker(i) {
    if (pool[i]) {
      try {
        // new connection
        const sqs = new AWS.SQS({
          httpOptions: { timeout: 11 * 1000 },
          apiVersion: "2012-11-05",
        });
        process(i, sqs);
      } catch (err) {
        err.message = `Error to get sqs messages': ${err.message}`;
        logger.error(err);
      }
    }
  }

  setInterval(() => {
    for (let i = 0; i < 10; i++) {
      worker(i);
    }
  }, 0);
};

module.exports = mainFunction;
