const AWS = require("aws-sdk");
const json = require("../myJsonFile");
require("dotenv").config();
const getSSMParameters = require("../config/index");

const sendByOne = async () => {
  const receivedParams = await getSSMParameters();
  console.log(receivedParams.Parameters);
  AWS.config.update({
    region: receivedParams.Parameters[3].Value,
    accessKeyId: receivedParams.Parameters[0].Value, // should be:  process.env.AWS_ACCESS_ID
    secretAccessKey: receivedParams.Parameters[4].Value,
  });
  // Create SQS service client
  const sqs = new AWS.SQS({ apiVersion: "2012-11-05" });
  // Replace with your account id and the queue name you setup
  const accountId = receivedParams.Parameters[1].Value;
  const queueName = receivedParams.Parameters[2].Value;

  const sendByOne = () => {
    const array = json.map((message, index) => {
      const params = {
        MessageBody: message.MessageBody,
        QueueUrl: `https://sqs.eu-central-1.amazonaws.com/${accountId}/${queueName}`,
      };
      return sqs.sendMessage(params, (err, data) => {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Successfully added message", data.MessageId);
        }
      });
    });
    Promise.all(array).then(console.log);
  };
  sendByOne();
};

module.exports = sendByOne;
