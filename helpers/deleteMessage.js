const sleep = require("../helpers/sleep");
const AWS = require("aws-sdk");
const sqs = new AWS.SQS({ apiVersion: "2012-11-05" });
const deleteMessage = async (QueueUrl, ReceiptHandle, Id) => {
  console.log("deleted ", Id);
  return sqs.deleteMessage({ QueueUrl, ReceiptHandle }).promise();
};

module.exports = deleteMessage;
