require("dotenv").config();

const ssm = new (require("aws-sdk/clients/ssm"))();

const getSSMParameters = async () => {
  const data = await ssm
    .getParameters({
      Names: [`ACCESS_ID`, `ACCOUNT_ID`, `QUEUE_NAME`, `REGION`, `SECRET_KEY`],
    })
    .promise();
  return data;
};
module.exports = getSSMParameters;
